const { createApp } = Vue;

const toastShowTime = 5000;

const app = createApp({
  data() {
    return {
      showProfileDropdown: false,
      showMessagesDropdown: false,
      showMobileMenu: false,

      showLoginPopup: false,
      showRoomPopup: false,
      showInvitePopup: false,
      showDirectPopup: false,

      username: "",
      rooms: [],
      roomTitle: "",
      friendUsername: "",

      socket: null,
      messages: [],
      selectedRoom: null,
      selectedRoomId: "",
      currentMessage: "",

      usernameToInvite: "",
    };
  },
  computed: {
    isLoggedIn() {
      return !!localStorage.getItem("username");
    },
    localUsername() {
      return localStorage.getItem("username");
    },
  },
  watch: {
    isLoggedIn: {
      handler(val) {
        if (val === true) {
          this.getRooms();
        }
      },
      immediate: true,
    },
    selectedRoomId: {
      handler(val) {
        this.leaveRoom();
        this.joinToRoom(val);
      },
    },
  },
  methods: {
    toggleMessagesDropdown(value) {
      this.showMessagesDropdown = value;
    },
    toogleProfileDropDown(value) {
      this.showProfileDropdown = value;
    },
    toggleMobileMenu(value) {
      this.showMobileMenu = value;
    },
    toggleLoginPopup(value) {
      this.showLoginPopup = value;
    },
    toggleRoomPopup(value) {
      this.showRoomPopup = value;
    },
    toggleInvitePopup(value) {
      this.showInvitePopup = value;
    },
    toggleDirectPopup(value) {
      this.showDirectPopup = value;
    },
    async login() {
      if (this.username.trim() === "") {
        return;
      }
      try {
        const user = await axios.get(
          `http://localhost:3000/auth/${this.username}`
        );
        localStorage.setItem("username", user.data.username);
        this.showLoginPopup = false;
        this.isLoggedIn = true;
        this.getRooms();
      } catch (e) {
        toast = new Toast({
          message: e?.message,
          type: "error",
        });
        setTimeout(() => {
          toast._close();
        }, toastShowTime);
      }
    },
    async getRooms() {
      try {
        const rooms = await axios.get(
          `http://localhost:3000/rooms/${localStorage.getItem("username")}`
        );
        this.rooms = rooms.data;
      } catch (e) {
        toast = new Toast({
          message: e?.message,
          type: "error",
        });
        setTimeout(() => {
          toast._close();
        }, toastShowTime);
      }
    },
    async createRoom(isDirect) {
      if (!isDirect && this.roomTitle.trim() === "") {
        return;
      }
      if (isDirect && this.friendUsername.trim() === "") {
        return;
      }
      let title = this.roomTitle;
      if (isDirect) {
        console.log(123);
        try {
          const user = await axios.get(
            "http://localhost:3000/auth/" + this.friendUsername
          );
          title = user.data.full_name;
        } catch (e) {
          toast = new Toast({
            message: "User not found",
            type: "error",
          });
          setTimeout(() => {
            toast._close();
          }, toastShowTime);
          return;
        }
      }
      try {
        const {data: room } = await axios.post("http://localhost:3000/rooms", {
          username: localStorage.getItem("username"),
          title,
          isDirect,
        });
        if(isDirect) {
          await axios.put("http://localhost:3000/rooms", {
          username: this.friendUsername,
          room_id: room._id,
        });
        }
        this.getRooms();
        this.showRoomPopup = false;
        this.showDirectPopup = false;
      } catch (e) {
        toast = new Toast({
          message: e?.message,
          type: "error",
        });
        setTimeout(() => {
          toast._close();
        }, toastShowTime);
      }
    },
    joinToRoom(roomId) {
      this.selectedRoom = null;
      const username = localStorage.getItem("username");

      if (!username) {
        return;
      }

      if (this.socket?.connected) {
        this.leaveRoom();
      }
      this.socket = io("http://localhost:3000", {
        extraHeaders: {
          room: roomId,
        },
      });

      this.selectedRoomId = roomId;
      this.selectedRoom = this.rooms.find((item) => {
        return item._id === roomId;
      });
      console.log(this.selectedRoom);
      if (!this.selectedRoom) {
        return;
      }

      this.socket.once("load_messages", (data) => {
        this.messages = [];
        this.messages = data;
        console.log(data);
      });

      this.socket.on("new_message", (data) => {
        this.messages.push(data);
        return;
      });
    },
    async inviteUser() {
      if (this.usernameToInvite.trim() === "") {
        return;
      }
      try {
        await axios.put("http://localhost:3000/rooms", {
          username: this.usernameToInvite,
          room_id: this.selectedRoom._id,
        });
        this.usernameToInvite = "";
        this.showInvitePopup = false;
      } catch (e) {
        toast = new Toast({
          message: e?.message,
          type: "error",
        });
        setTimeout(() => {
          toast._close();
        }, toastShowTime);
      }
    },
    sendMessage() {
      if (this.currentMessage?.trim() === "" && !this.localUsername) {
        return;
      }
      console.log(this.localUsername);
      this.socket.emit("send_message", {
        username: this.localUsername,
        room_id: this.selectedRoom._id,
        content: this.currentMessage,
      });
      this.currentMessage = "";
    },
    leaveRoom() {
      this.socket.disconnect();
      this.messages = [];
      console.log("disconnected");
    },
  },
  mounted() {
    const username = localStorage.getItem("username");
    this.showLoginPopup = !!!username;
  },
  destroyed() {
    this.leaveRoom();
  },
}).mount("#app");
