let studentsArray = [];
let studIdToEdit = -1;


const bellIcon = document.getElementsByClassName("fa-bell")[0];
const notificationCircle = document.getElementsByTagName("svg")[0];
const userProfileSubMenu = document.getElementById("user-profile-submenu");
const notificationsSubMenu = document.getElementById('notification-submenu');

const notificationsIcon = document.getElementById('notification-info');
const userProfileImg = document.querySelector(".user-profile");

const openAddStudPopUpBtn = document.getElementById("btn-show-add-window");
const closeAddEditStudPopUpBtn = document.getElementById("add-edit-form-close-btn");
const addEditStudPopUp = document.getElementById("pop-up-add-edit-student");
const okAddEditStudPopUpBtn = document.getElementById("ok-add-edit-student-btn");
const addEditStudPopUpBtn = document.getElementById("create-edit-student-btn");
const addEditStudForm = document.getElementById("form-add-edit-student");
const studentsTableId = document.getElementById("studentsTable");
const studentsTableBody = studentsTableId.getElementsByTagName('tbody')[0];
let popUpAddEditTitle = document.getElementById("add-edit-pop-up-h3");
let studIndex = 0;
let editTr = null;

const closeDeleteStudPopUp = document.getElementById("delete-form-close-btn");
const deleteStudPopUp = document.getElementById("pop-up-delete-student");
const cancelDeleteStudBtn = document.getElementById("cancel-delete-student-btn");
const okDeleteStudBtn = document.getElementById("ok-delete-student-btn");
let deleteTr = null;

let studTableCheckboxesArray = document.getElementsByClassName("student-table-checkbox");
let studTableCheckboxesCheckedCounter = 0;

const studGroupSelect = document.getElementById("student-groups-select");
const studFirstNameInput = document.getElementById("student-first-name");
const studLastNameInput = document.getElementById("student-last-name");
const studGenderSelect = document.getElementById("student-genders-select");
const studBirthdayInput = document.getElementById("student-birthday");

const toastShowTime = 5000;
let toast = null;

// Notification section
bellIcon.addEventListener("dblclick", () => {
    bellIcon.style.animation = "shake 1s linear 2";
    notificationCircle.style.animation = "jumping-dot 2s linear 2";
})

//User profile section
userProfileImg.addEventListener("mouseenter", function(){
    userProfileSubMenu.classList.add("submenu-active");
})

userProfileImg.addEventListener("mouseleave", function(){
    userProfileSubMenu.classList.remove("submenu-active");
})

notificationsIcon.addEventListener('mouseenter', function() {
    notificationsSubMenu.classList.add('submenu-active');
})

notificationsIcon.addEventListener('mouseleave', function() {
    notificationsSubMenu.classList.remove('submenu-active'); 
})

$(document).ready(function() {
    getStudentsArray().then((data) => {
        studentsArray = data;
        renderTable();
    })
})


function renderTable() {
    clearTable()
    studentsArray?.forEach(item => {
        insertStudentIntoTable(item);
    })
    renewTableCheckboxes();
}

function clearTable() {
    studentsTableBody.innerHTML = ""
}

// Clear studAddEdit form fields after clicking on Submit button
function clearStudAddFormFields(){
    studGroupSelect.value = "";
    studFirstNameInput.value = "";
    studLastNameInput.value = "";
    studGenderSelect.value = "";
    studBirthdayInput.value = "";
} 

function insertStudentIntoTable(student) {
    const newRow = studentsTableBody.insertRow(-1);
    newRow.setAttribute('data-id', student.id);

    const checkboxCell = newRow.insertCell(0);
    checkboxCell.setAttribute('class', 'student-table-text');
    const checkboxInput = document.createElement('input');
    checkboxInput.setAttribute('type', 'checkbox');
    checkboxInput.setAttribute('class', 'student-table-checkbox');
    checkboxInput.setAttribute('name', '');
    checkboxInput.setAttribute('value', '');
    checkboxCell.appendChild(checkboxInput);

    const groupCell = newRow.insertCell(1);
    groupCell.setAttribute('class', 'student-table-text');
    groupCell.innerHTML = student.group;

    const nameCell = newRow.insertCell(2);
    nameCell.setAttribute('class', 'student-table-text');
    nameCell.innerHTML = student.first_name + ' ' + student.last_name;

    const genderCell = newRow.insertCell(3);
    genderCell.setAttribute('class', 'student-table-text');
    genderCell.innerHTML = student.gender;

    const birthdayCell = newRow.insertCell(4);
    birthdayCell.setAttribute('class', 'student-table-text');
    birthdayCell.innerHTML = student.birthday;

    const iconCell = newRow.insertCell(5);
    const icon = document.createElement('i');
    icon.setAttribute('class', 'fa-solid fa-circle active-user');
    iconCell.appendChild(icon);

    const buttonCell = newRow.insertCell(6);
    const editButton = document.createElement('button');
    editButton.setAttribute('type', 'button');
    editButton.setAttribute('class', 'student-edit-btn');
    const editIcon = document.createElement('i');
    editIcon.setAttribute('class', 'fa-light fa-pen fa');
    editButton.appendChild(editIcon);
    buttonCell.appendChild(editButton);

    const deleteButton = document.createElement('button');
    deleteButton.setAttribute('type', 'button');
    deleteButton.setAttribute('class', 'student-delete-btn');
    const deleteIcon = document.createElement('i');
    deleteIcon.setAttribute('class', 'fa-solid fa-xmark');
    deleteButton.appendChild(deleteIcon);
    buttonCell.appendChild(deleteButton);
}

// Adding students section
openAddStudPopUpBtn.addEventListener('click', function(e) {
    e.preventDefault();
    setDateRestrictions();
    popUpAddEditTitle.innerHTML = "Add Student";
    addEditStudPopUpBtn.value = "Create";
    addEditStudPopUp.classList.add('active');
})

closeAddEditStudPopUpBtn.addEventListener('click', ()=> {
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
})


// Adding a new empty table row when click 'Ok' button (Add student) or just close the pop-up (Edit student) 
okAddEditStudPopUpBtn.addEventListener('click', function(e){
    e.preventDefault();


    if(popUpAddEditTitle.innerHTML === "Add Student") {
        // Adding a new empty row to the students table
        const newRow = studentsTableBody.insertRow(-1);
        newRow.setAttribute('data-id', studentsArray[studentsArray.length - 1].id);

        const checkboxCell = newRow.insertCell(0);
        checkboxCell.setAttribute('class', 'student-table-text');
        const checkboxInput = document.createElement('input');
        checkboxInput.setAttribute('type', 'checkbox');
        checkboxInput.setAttribute('class', 'student-table-checkbox');
        checkboxInput.setAttribute('name', '');
        checkboxInput.setAttribute('value', '');
        checkboxCell.appendChild(checkboxInput);

        const groupCell = newRow.insertCell(1);
        groupCell.setAttribute('class', 'student-table-text');
        groupCell.innerHTML = '';

        const nameCell = newRow.insertCell(2);
        nameCell.setAttribute('class', 'student-table-text');

        const genderCell = newRow.insertCell(3);
        genderCell.setAttribute('class', 'student-table-text');

        const birthdayCell = newRow.insertCell(4);
        birthdayCell.setAttribute('class', 'student-table-text');

        const iconCell = newRow.insertCell(5);
        const icon = document.createElement('i');
        icon.setAttribute('class', 'fa-solid fa-circle active-user');
        iconCell.appendChild(icon);

        const buttonCell = newRow.insertCell(6);
        const editButton = document.createElement('button');
        editButton.setAttribute('type', 'button');
        editButton.setAttribute('class', 'student-edit-btn');
        const editIcon = document.createElement('i');
        editIcon.setAttribute('class', 'fa-light fa-pen fa');
        editButton.appendChild(editIcon);
        buttonCell.appendChild(editButton);

        const deleteButton = document.createElement('button');
        deleteButton.setAttribute('type', 'button');
        deleteButton.setAttribute('class', 'student-delete-btn');
        const deleteIcon = document.createElement('i');
        deleteIcon.setAttribute('class', 'fa-solid fa-xmark');
        deleteButton.appendChild(deleteIcon);
        buttonCell.appendChild(deleteButton);

        // Convert a JS object into a string with JSON.stringify()
        const studentJSON = JSON.stringify(studentsArray[studentsArray.length - 1]);
        console.log("Added student: " + studentJSON);
        renewTableCheckboxes();
        addEditStudPopUp.classList.remove('active');
    } else if(popUpAddEditTitle.innerHTML === "Edit Student") {
        console.log("Edit Student");
    }
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
})

// Adding or editing a student depending on title value
addEditStudForm.addEventListener('submit', onAddEditStudent);

// Function for adding a new student when click 'Create' button
async function onAddStudent(e){
    e.preventDefault();

    const studentGroup = document.getElementById("student-groups-select").value;
    const studentFirstName = document.getElementById("student-first-name").value;
    const studentLastName = document.getElementById("student-last-name").value;
    const studentGender = document.getElementById("student-genders-select").value;
    const studentBirthday = document.getElementById("student-birthday").value;
    const dateList = studentBirthday.split('-');
    const newStudentBirthday = dateList[2] + '.' + dateList[1] + '.' + dateList[0];

    try{
        let sendRes = await sendStudent({
            first_name: studentFirstName,
            last_name: studentLastName,
            group: studentGroup,
            birthday: newStudentBirthday,
            gender: studentGender
        });
        studentsArray = await getStudentsArray();
        renderTable();
        renewTableCheckboxes();
        addEditStudPopUp.classList.remove('active');
        clearStudAddFormFields();
        toast = new Toast({
            message: `Successfully added student ${sendRes.first_name} ${sendRes.last_name}`,
            type: 'success'
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    } catch(e) {
        toast = new Toast({
            message: `${e.responseJSON?.message}`,
            type: 'danger'
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    }
}

// Function for changing info about student when click 'Save' button
async function onEditStudent(e){
    e.preventDefault();
    const studentGroup = document.getElementById("student-groups-select").value;
    const studentFirstName = document.getElementById("student-first-name").value;
    const studentLastName = document.getElementById("student-last-name").value;
    const studentGender = document.getElementById("student-genders-select").value;
    const studentBirthday = document.getElementById("student-birthday").value;
    const dateList = studentBirthday.split('-');
    const newStudentBirthday = dateList[2] + '.' + dateList[1] + '.' + dateList[0];
    if(studIdToEdit === -1) {
        addEditStudPopUp.classList.remove('active');
        clearStudAddFormFields();
        return;
    }
    try {
        const editRes = await editStudent(+studIdToEdit, {
            first_name: studentFirstName,
            last_name: studentLastName,
            gender: studentGender,
            group: studentGroup,
            birthday: newStudentBirthday,
        })
        const students = await getStudentsArray();
        studentsArray = students;
        renderTable();
        addEditStudPopUp.classList.remove('active');
        clearStudAddFormFields();
        toast = new Toast({
            message: `Successfully edited student ${editRes.first_name} ${editRes.last_name}`,
            type: 'success',
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    } catch(e) {
        toast = new Toast({
            message: `${e.responseJSON?.message}`,
            type: 'danger',
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    }
}


function onAddEditStudent (e){
    if(popUpAddEditTitle.innerHTML === "Add Student") {
        onAddStudent(e);
    } else if(popUpAddEditTitle.innerHTML === "Edit Student") {
        onEditStudent(e);
    }
}

// Editing students sections
async function onEditStudRow(e) {
    if(e.target.id === 'btn-show-add-window'){
        return;
    } else if(!e.target.classList.contains('student-edit-btn')){
        return;
    } else if(studTableCheckboxesCheckedCounter > 1) {
        return;
    }
    e.preventDefault();
    setDateRestrictions();
    popUpAddEditTitle.innerHTML = "Edit Student";
    addEditStudPopUpBtn.value = "Save";
    addEditStudPopUp.classList.add('active');
    const editBtn = e.target;
    editTr = editBtn.closest('tr');
    studIndex = editTr.rowIndex - 1;

    const editStudGroupSelect = document.getElementById("student-groups-select");
    const editStudFirstNameField = document.getElementById("student-first-name");
    const editStudLastNameField = document.getElementById("student-last-name");
    const editStudGenderSelect = document.getElementById("student-genders-select");
    const editStudBirthdaySelect = document.getElementById("student-birthday");

    const id = studentsArray[studIndex].id;

    const student = await getStudent(id);
    editStudGroupSelect.value = student.group;
    editStudFirstNameField.value = student.first_name;
    editStudLastNameField.value = student.last_name;
    editStudGenderSelect.value = student.gender;
    const dateList = student.birthday.split('.');
    const newStudBirthday = dateList[2] + '-' + dateList[1] + '-' + dateList[0];
    editStudBirthdaySelect.value = newStudBirthday;

    studIdToEdit = student.id;
}

studentsTableBody.addEventListener('click', onEditStudRow);


// Deleting students section
async function onDeleteStudRow(e) {
    if(!e.target.classList.contains('student-delete-btn')){
        return;
    } else if(studTableCheckboxesCheckedCounter > 1 && $("#studentsTableHeader-checkbox")[0].checked !== true) {
        return;
    }
    e.preventDefault();

    if($("#studentsTableHeader-checkbox")[0].checked === true) {
        let studentsIdToDeleteArray = [];
        studentsArray.forEach((student) => {
            studentsIdToDeleteArray.push(student.id);
        })
        try{
            const deleteRes = await deleteStudents([...studentsIdToDeleteArray]);
            studentsArray = await getStudentsArray();
            renderTable();
            toast = new Toast({
                message: `${deleteRes.message}`,
                type: 'success',
            })
            setTimeout(() => {
                toast._close()
            }, toastShowTime)
            $("#studentsTableHeader-checkbox")[0].checked = false
        } catch(e) {
            toast = new Toast({
                message: `${e.responseJSON?.message}`,
                type: 'danger',
            })
            setTimeout(() => {
                toast._close()
            }, toastShowTime)
        }   
    } else {
        deleteStudPopUp.classList.add('active');
        const deleteBtn = e.target;
        deleteTr = deleteBtn.closest('tr');
        const studData = deleteTr.getElementsByTagName("td");
        const studName = studData[2].innerHTML;
        deleteStudParagraph = document.getElementById("p-delete-student");
        deleteStudParagraph.innerHTML = "Are you sure you want to delete user " + studName + " ?";
    }

}

async function confirmDeletion() {
    studIndex = deleteTr.rowIndex - 1;
    const id = studentsArray[studIndex].id;
    try{
        const deleteRes = await deleteStudents([+id]);
        studentsArray = await getStudentsArray();
        renderTable();
        toast = new Toast({
            message: `${deleteRes.message}`,
            type: 'success',
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    } catch(e) {
        toast = new Toast({
            message: `${e.responseJSON?.message}`,
            type: 'danger',
        })
        setTimeout(() => {
            toast._close()
        }, toastShowTime)
    }
    
    deleteStudPopUp.classList.remove('active');
    studTableCheckboxesCheckedCounter = 0;
    addEditStudPopUp.classList.remove('active');
}

studentsTableId.addEventListener('click', onDeleteStudRow);

closeDeleteStudPopUp.addEventListener('click', ()=> {
    deleteStudPopUp.classList.remove('active');
})

cancelDeleteStudBtn.addEventListener('click', () => {
    deleteStudPopUp.classList.remove('active');
})

function onBurgerClick() {
   const navbarMobile = document.getElementById('navbar-mobile');
   navbarMobile.classList.add('mobile-active');
}

function onModalClose() {
    const navbarMobile = document.getElementById('navbar-mobile');
    navbarMobile.classList.remove('mobile-active');
}

// Checkboxes area
$("#studentsTableHeader-checkbox").change(function(){
    if(this.checked === true){
        studTableCheckboxesCheckedCounter = 0;
        $(".student-table-checkbox").each(function(){
            this.checked = true;
        });
        studTableCheckboxesCheckedCounter += studTableCheckboxesArray.length - 1;
    } 
    else {
        $(".student-table-checkbox").each(function(){
            this.checked = false;
        });
        studTableCheckboxesCheckedCounter -= studTableCheckboxesArray.length - 1;
    }
})


function onCheckboxChange(event) {
    item = event.currentTarget;

    if(item.checked === true){
        studTableCheckboxesCheckedCounter += 1;
    } else {
        studTableCheckboxesCheckedCounter -= 1;
        $("#studentsTableHeader-checkbox")[0].checked = false;
    }
    if(studTableCheckboxesCheckedCounter === (studTableCheckboxesArray.length - 1)) {
        $("#studentsTableHeader-checkbox")[0].checked = true;
    }
    console.log("Number of checked checkboxes = " + studTableCheckboxesCheckedCounter);
}

function renewTableCheckboxes() {
    studTableCheckboxesCheckedCounter = 0;
    $("#studentsTableHeader-checkbox")[0].checked = false;
    for(let i = 1; i<studTableCheckboxesArray.length; i++) {
        studTableCheckboxesArray[i].addEventListener("change", onCheckboxChange);
    }
}

// Register a service worker 
/*if('serviceWorker' in navigator){
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/sw.js').then(registration => {
            //Registration was successful
            console.log('ServiceWorker registred ${registration.scope}');
        }, err => {
            //Registration failed
            console.log('ServiceWorker registration failed, ${err}');
        });
    }); 
}*/


// Set date restrictions
function setDateRestrictions(){
    const today = new Date();
    const todayYear = today.getFullYear();
    const todayMonth = String(today.getMonth() + 1).padStart(2, '0'); // Months are zero indexed, so we add 1 and pad with zeroes if needed
    const todayDay = String(today.getDate()).padStart(2, '0'); // Pad with zeroes if needed
 
    studBirthdayInput.setAttribute("min", `${todayYear - 100}-${todayMonth}-${todayDay}`);
    studBirthdayInput.setAttribute("max", `${todayYear - 15}-${todayMonth}-${todayDay}`);
}

