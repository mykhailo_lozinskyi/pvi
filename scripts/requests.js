async function getStudentsArray()
{
    return await $.ajax({
        url: "http://localhost:8000/api/students/",
        method: 'get'
      })
}

async function getStudent(id)
{
    return await $.ajax({
        url: `http://localhost:8000/api/students/${id}`,
        method: 'get'
    })
}

async function sendStudent(payload)
{
    return await $.ajax({
            url: 'http://localhost:8000/api/students/',
            type: 'POST',
            dataType: 'json',
            data: payload
        })
}

async function editStudent(id, payload)
{
    return await $.ajax({
        url: 'http://localhost:8000/api/students/' + id,
        type: 'PUT',
        dataType: 'json',
        data: payload
    })
}

async function deleteStudents(ids)    
{
    console.log(ids);
    return await $.ajax({
        url: 'http://localhost:8000/api/students/',
        type: 'DELETE',
        dataType: 'json',
        data: {
          ids
        }
    })
}
