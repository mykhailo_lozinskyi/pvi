/*const staticCacheName = 'site-static-v1';
const dynamicCacheName = 'site-dynamic-v1';
const assets = [
    '/',
    'students.html',
    '/images/user_1.png',
    '/images/profile_placeholder_msg2.png',
    '/css/styles.css',
    '/css/students_header.css',
    '/css/students_navbar.css',
    '/css/students_table.css',
    '/css/students_pagination.css',
    '/css/students_popups.css',
    '/css/students_media.css',
    '/scripts/main.js',
    '/pages/fallback.html',
    '/css/fallback_page.css',
    '/manifest.json',
    'css/tost.min.css',
    'scripts/requests.js',
    'scripts/tost.min.js'
];

// Install service worker
self.addEventListener('install', evt => {
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('Caching shell assets');
            cache.addAll(assets);
        })
    );
});

//Activate event
self.addEventListener('activate', evt => {
    evt.waitUntil(
        caches.keys().then(keys => {
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))
            )
        })
    );
});

//Fetch event
self.addEventListener('fetch', event => {
    event.respondWith(caches.open(dynamicCacheName).then( async cache => {
        const staticCache = await caches.open(staticCacheName)
        const staticCachedReponse = await staticCache.match(event.request.url)
        if(!staticCachedReponse) {
            const cachedResponse = await cache.match(event.request.url);
            if (cachedResponse) {
                return cachedResponse;
            }
            try {
                const fetchedResponse = await fetch(event.request);
                // Add the network response to the cache for later visits
                cache.put(event.request, fetchedResponse.clone());
                return fetchedResponse;
            } catch (e) {
                return staticCache.match('/pages/fallback.html')
            }
        } else {
            return staticCachedReponse
        }
    }))
});*/

